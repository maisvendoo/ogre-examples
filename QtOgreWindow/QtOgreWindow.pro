#-------------------------------------------------------------------------------
#
#       �������� ������ ��� qmake
#
#
#-------------------------------------------------------------------------------

# ��� ������ � ��� ����������
TEMPLATE = lib
TARGET = QtOgreWindow

# ���������� ������ Qt
QT += core gui
QT += widgets

# ������������ ������ ��� unix
unix {

    INCLUDEPATH += /usr/include/OGRE
    INCLUDEPATH += /usr/include/OGRE/Overlay    

    CONFIG += link_pkgconfig
    PKGCONFIG += OGRE
}

# ������������ ������ ��� �����
win32 {

}


CONFIG(debug, debug | release) {
    # ������ ������� ��� ����������� ���������
    TARGET = $$join(TARGET,,,_d)
    # ���������� ������ ���������
    LIBS *= -lOgreMain -lOIS -lOgreOverlay -lboost_system
    # ���� � ������������ �����
    DESTDIR = ../lib

} else {
    # ����������� ��������� ��� ������
    LIBS *= -lOgreMain -lOIS -lOgreOverlay -lboost_system
    DESTDIR = ../lib
}

# ���� ������ ����������
INCLUDEPATH += include/

# ������ ������������ ������
HEADERS += $$files(include/*.h)

# ��������� �������
SOURCES += $$files(src/*.cpp)
