//------------------------------------------------------------------------------
//
//      ������� ���������� ������ ��� ������ Qt + Ogre
//      (c) �������� �.�., 06/01/2017
//
//------------------------------------------------------------------------------
#ifndef QTCAMERAMAN_H
#define QTCAMERAMAN_H

#include    <OgreCamera.h>
#include    <OgreSceneNode.h>
#include    <OgreSceneManager.h>
#include    <OgreFrameListener.h>

#include    <QKeyEvent>
#include    <QMouseEvent>

#if defined QTCAMERAMAN
#define QTCAMERAMAN_COMMON_DLLSPEC Q_DECL_EXPORT
#else
#define QTCAMERAMAN_COMMON_DLLSPEC Q_DECL_IMPORT
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace OgreQtBites
{
    enum CameraStyle
    {
        CS_FREELOOK,
        CS_ORBIT,
        CS_MANUAL
    };

    class QTCAMERAMAN_COMMON_DLLSPEC QtCameraMan
    {
    public:

        QtCameraMan(Ogre::Camera *cam);
        virtual ~QtCameraMan();

        // ��������� � ��������� ��������� �� ������
        virtual void setCamera(Ogre::Camera *cam);
        virtual Ogre::Camera *getCamera();

        // ��������� ����, ������� ������������ ������
        virtual void setTarget(Ogre::SceneNode *target);
        virtual Ogre::SceneNode *getTarget();

        // ���������� ��������, ������ ������ � ��������� �� ����
        virtual void setYawPitchDist(Ogre::Radian yaw,
                                     Ogre::Radian pitch,
                                     Ogre::Real dist);

        // ���������� ������������ �������� ������
        virtual void setTopSpeed(Ogre::Real topSpeed);
        virtual Ogre::Real getTopSpeed();

        // ���������� ����� ������
        virtual void setStyle(CameraStyle style);
        virtual CameraStyle getStyle();

        // ���������� ������
        virtual void manualStop();

        // ���������� ����������� ������
        virtual bool frameRenderingQueued(const Ogre::FrameEvent &evt);

        // ����������� ������� �����
        virtual void injectKeyDown(const QKeyEvent &evt);
        virtual void injectKeyUp(const QKeyEvent &evt);
        virtual void injectMouseMove(int relX, int relY);
        virtual void injectWheelMove(const QWheelEvent &evt);
        virtual void injectMouseDown(const QMouseEvent &evt);
        virtual void injectMouseUp(const QMouseEvent &evt);

    protected:

        Ogre::Camera        *mCamera;
        CameraStyle         mStyle;
        Ogre::SceneNode     *mTarget;

        bool                mOrbiting;
        bool                mZooming;

        Ogre::Real          mTopSpeed;
        Ogre::Vector3       mVelocity;

        // ����� ����������� �������� ������
        bool                mForward;
        bool                mBack;
        bool                mLeft;
        bool                mRight;
        bool                mUp;
        bool                mDown;

        // ���� ����������� �����������
        bool                mFastMove;
    };
}

#endif // QTCAMERAMAN_H
