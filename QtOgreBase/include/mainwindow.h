//------------------------------------------------------------------------------
//
//
//
//
//------------------------------------------------------------------------------
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include    "qtogrebase.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class MainWindow : public QtOgreBaseWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWindow *parent = NULL,
                        int width = DEFAULT_WIDTH,
                        int height = DEFAULT_HEIGHT);

    virtual ~MainWindow();

protected:

    virtual void createScene();
    virtual void createCamera();

    virtual bool frameRenderingQueued(const Ogre::FrameEvent &evt);

    virtual void keyPressEvent(QKeyEvent *ev);
    virtual void keyReleaseEvent(QKeyEvent *ev);
};

#endif // MAINWINDOW_H
