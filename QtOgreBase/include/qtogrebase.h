//------------------------------------------------------------------------------
//
//
//
//
//------------------------------------------------------------------------------
#ifndef QTOGREBASE_H
#define QTOGREBASE_H

#include    <iostream>

#include    <QtWidgets/QApplication>
#include    <QtGui/QKeyEvent>
#include    <QtGui/QWindow>

#include    <Ogre.h>
#include    <OgreOverlaySystem.h>

#include    "qtcameraman.h"

// ������� ���� ��-���������
#define     DEFAULT_WIDTH   1024
#define     DEFAULT_HEIGHT  768

using namespace std;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class QtOgreBaseWindow : public QWindow, public Ogre::FrameListener
{
    Q_OBJECT

public:

    // ����������� ������
    explicit QtOgreBaseWindow(QWindow *parent = NULL,
                              int width = DEFAULT_WIDTH,
                              int height = DEFAULT_HEIGHT);

    // ���������� ������
    virtual ~QtOgreBaseWindow();

    // �������������� ����� ����������� ����
    virtual void show(bool fullscreen = false);

    //
    void setAnimating(bool animating);

public slots:

    // ����� ������� Qt
    virtual bool eventFilter(QObject *watched, QEvent *event);

    //---- �����, ��������� � ������� Ogre ----
    virtual void renderLater();
    virtual void renderNow();

protected:

    //---- ���������������� ������ QWindow ----

    // ���������� ����� ������� �������
    virtual void keyPressEvent(QKeyEvent *ev);
    // ���������� ����� ���������� �������
    virtual void keyReleaseEvent(QKeyEvent *ev);
    // ���������� ����������� ����
    virtual void mouseMoveEvent(QMouseEvent *e);
    // ���������� ������ ����
    virtual void wheelEvent(QWheelEvent *e);
    // ���������� ������� ������ ����
    virtual void mousePressEvent(QMouseEvent *e);
    // ���������� ���������� ������ ����
    virtual void mouseReleaseEvent(QMouseEvent *e);

    // ���������� ��������� ���� ����� �������� (���������� ��������)
    virtual void exposeEvent(QExposeEvent *event);
    // ���������� ������� ����
    virtual bool event(QEvent *event);

    //---- �������, ��������� � ����������� Ogre ----
    Ogre::RenderWindow          *mWindow;
    Ogre::Root                  *mRoot;
    bool                        m_update_pending;
    bool                        m_animating;

    Ogre::SceneManager          *mSceneMgr;
    Ogre::OverlaySystem         *mOverlaySystem;

    Ogre::Camera                *mCamera;

    OgreQtBites::QtCameraMan    *mCameraMan;

    //---- ������, ��������� � ����������� Ogre ----

    // �������������
    virtual bool init();
    // ��������� ������� ���������� � �������� ����
    virtual bool configure();
    // ��������� ��������� ��������
    virtual void setupResources();
    // �������� � ��������� ��������� �����
    virtual void chooseSceneManager();
    // �������� � ��������������� ��������� ������
    virtual void createCamera();
    // ��������� ��������
    virtual void createViewPort();
    //
    virtual void createResourceListener();
    // �������� ��������
    virtual void loadResources();
    // �������� �����, ������������� � ����������
    virtual void createScene() = 0;
    //
    virtual void createFrameListener();
    //
    virtual bool frameRenderingQueued(const Ogre::FrameEvent &evt);

    virtual void render(QPainter *painter);
    virtual void render();

    void log(Ogre::String msg);
    void log(QString msg);
};

#endif // QTOGREBASE_H
