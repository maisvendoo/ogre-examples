//------------------------------------------------------------------------------
//
//
//
//
//------------------------------------------------------------------------------
#include    "qtcameraman.h"

using namespace OgreQtBites;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
QtCameraMan::QtCameraMan(Ogre::Camera *cam) :
    mCamera(0),
    mTarget(0),
    mOrbiting(false),
    mZooming(false),
    mTopSpeed(150),
    mVelocity(Ogre::Vector3::ZERO),
    mForward(false),
    mBack(false),
    mLeft(false),
    mRight(false),
    mUp(false),
    mDown(false),
    mFastMove(false)

{
    // ������������� ������
    setCamera(cam);
    // ������������� ����� ������
    setStyle(CS_FREELOOK);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
QtCameraMan::~QtCameraMan()
{

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::setCamera(Ogre::Camera *cam)
{
    mCamera = cam;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
Ogre::Camera *QtCameraMan::getCamera()
{
    return mCamera;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::setTarget(Ogre::SceneNode *target)
{
    // ���� ���������� ���� �� ��������� � �������
    if (target != mTarget)
    {
        // ������ ����,
        mTarget = target;

        // ���� ���� ����������
        if (mTarget)
        {
            // ������������� ���������� ������ � ���������� �� ����
            setYawPitchDist(Ogre::Degree(0), Ogre::Degree(15), 150);
            // �������� ������������� ���� �������
            mCamera->setAutoTracking(true, mTarget);
        }
        else
        {
            // ��������� ������������� ����
            mCamera->setAutoTracking(false);
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
Ogre::SceneNode *QtCameraMan::getTarget()
{
    return mTarget;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::setYawPitchDist(Ogre::Radian yaw,
                                  Ogre::Radian pitch,
                                  Ogre::Real dist)
{
    mCamera->setPosition(mTarget->_getDerivedPosition());
    mCamera->setOrientation(mTarget->_getDerivedOrientation());
    mCamera->yaw(yaw);
    mCamera->pitch(-pitch);
    mCamera->moveRelative(Ogre::Vector3(0, 0, dist));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::setTopSpeed(Ogre::Real topSpeed)
{
    mTopSpeed = topSpeed;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
Ogre::Real QtCameraMan::getTopSpeed()
{
    return mTopSpeed;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::setStyle(CameraStyle style)
{
    if ( (mStyle != CS_ORBIT) && (style == CS_ORBIT) )
    {
        setTarget(mTarget ? mTarget :
                            mCamera->getSceneManager()->getRootSceneNode());

        mCamera->setFixedYawAxis(true);
        manualStop();
        setYawPitchDist(Ogre::Degree(0), Ogre::Degree(15), 150);
    }
    else if ( (mStyle != CS_FREELOOK) && (style == CS_FREELOOK) )
    {
        mCamera->setAutoTracking(false);
        mCamera->setFixedYawAxis(true);
    }
    else if ( (mStyle != CS_MANUAL) && (style == CS_MANUAL) )
    {
        mCamera->setAutoTracking(false);
        manualStop();
    }

    mStyle = style;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
CameraStyle QtCameraMan::getStyle()
{
    return mStyle;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::manualStop()
{
    if (mStyle == CS_FREELOOK)
    {
        mForward = false;
        mBack = false;
        mLeft = false;
        mRight = false;
        mUp = false;
        mDown = false;
        mVelocity = Ogre::Vector3::ZERO;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtCameraMan::frameRenderingQueued(const Ogre::FrameEvent &evt)
{
    if (mStyle == CS_FREELOOK)
    {
        Ogre::Vector3 accel = Ogre::Vector3::ZERO;

        if (mForward)
            accel += mCamera->getDirection();

        if (mBack)
            accel -= mCamera->getDirection();

        if (mRight)
            accel += mCamera->getRight();

        if (mLeft)
            accel -= mCamera->getRight();

        if (mUp)
            accel += mCamera->getUp();

        if (mDown)
            accel -= mCamera->getUp();

        Ogre::Real topSpeed = mFastMove ? mTopSpeed * 20 : mTopSpeed;

        if (accel.squaredLength() != 0)
        {
            accel.normalise();
            mVelocity += accel * topSpeed * evt.timeSinceLastFrame;
        }
        else
            mVelocity -= mVelocity * evt.timeSinceLastFrame;

        Ogre::Real tooSmall = std::numeric_limits<Ogre::Real>::epsilon();

        if (mVelocity.squaredLength() > topSpeed * topSpeed)
        {
            mVelocity.normalise();
            mVelocity *= topSpeed;
        }
        else if (mVelocity.squaredLength() < tooSmall * tooSmall)
        {
            mVelocity = Ogre::Vector3::ZERO;
        }

        if (mVelocity != Ogre::Vector3::ZERO)
            mCamera->move(mVelocity * evt.timeSinceLastFrame);
    }

    return true;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::injectKeyDown(const QKeyEvent &evt)
{
    if (mStyle == CS_FREELOOK)
    {
        if ( (evt.key() == Qt::Key_W) || (evt.key() == Qt::Key_Up) )
            mForward = true;
        else if ( (evt.key() == Qt::Key_S) || (evt.key() == Qt::Key_Down) )
            mBack = true;
        else if ( (evt.key() == Qt::Key_A) || (evt.key() == Qt::Key_Left) )
            mLeft = true;
        else if ( (evt.key() == Qt::Key_D) || (evt.key() == Qt::Key_Right) )
            mRight = true;
        else if (evt.key() == Qt::Key_PageUp)
            mUp = true;
        else if (evt.key() == Qt::Key_PageDown)
            mDown = true;
        else if (evt.key() == Qt::Key_Shift)
            mFastMove = true;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::injectKeyUp(const QKeyEvent &evt)
{
    if (mStyle == CS_FREELOOK)
    {
        if ( (evt.key() == Qt::Key_W) || (evt.key() == Qt::Key_Up) )
            mForward = false;
        else if ( (evt.key() == Qt::Key_S) || (evt.key() == Qt::Key_Down) )
            mBack = false;
        else if ( (evt.key() == Qt::Key_A) || (evt.key() == Qt::Key_Left) )
            mLeft = false;
        else if ( (evt.key() == Qt::Key_D) || (evt.key() == Qt::Key_Right) )
            mRight = false;
        else if (evt.key() == Qt::Key_PageUp)
            mUp = false;
        else if (evt.key() == Qt::Key_PageDown)
            mDown = false;
        else if (evt.key() == Qt::Key_Shift)
            mFastMove = false;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::injectMouseMove(int relX, int relY)
{
    if (mStyle == CS_ORBIT)
    {
        Ogre::Real dist = (mCamera->getPosition() -
                           mTarget->_getDerivedPosition()).length();

        if (mOrbiting)
        {
            mCamera->setPosition(mTarget->_getDerivedPosition());

            mCamera->yaw(Ogre::Degree(-relX * 0.025f));
            mCamera->pitch(Ogre::Degree(-relY * 0.025f));

            mCamera->moveRelative(Ogre::Vector3(0, 0, dist));
        }
        else if (mZooming)
        {
            mCamera->moveRelative((Ogre::Vector3(0, 0, relY * 0.004f * dist)));
        }
    }
    else if (mStyle == CS_FREELOOK)
    {
        mCamera->yaw(Ogre::Degree(-relX * 0.15f));
        mCamera->pitch(Ogre::Degree(-relY * 0.15f));
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::injectWheelMove(const QWheelEvent &evt)
{
    int relZ = evt.delta();

    if (mStyle == CS_ORBIT)
    {
        Ogre::Real dist = (mCamera->getPosition() -
                           mTarget->_getDerivedPosition()).length();

        if (relZ != 0)
        {
            mCamera->moveRelative(Ogre::Vector3(0, 0, -relZ * 0.0008f * dist));
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::injectMouseDown(const QMouseEvent &evt)
{
    if (mStyle == CS_ORBIT)
    {
        if (evt.buttons() & Qt::LeftButton)
            mOrbiting = true;
        else if (evt.buttons() & Qt::RightButton)
            mZooming = true;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtCameraMan::injectMouseUp(const QMouseEvent &evt)
{
    if (mStyle == CS_ORBIT)
    {
        if (evt.buttons() & Qt::LeftButton)
            mOrbiting = false;
        else if (evt.buttons() & Qt::RightButton)
            mZooming = false;
    }
}


