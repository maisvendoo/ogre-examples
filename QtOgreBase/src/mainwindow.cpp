//------------------------------------------------------------------------------
//
//
//
//
//------------------------------------------------------------------------------
#include    "mainwindow.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
MainWindow::MainWindow(QWindow *parent, int width, int height) :
    QtOgreBaseWindow(parent, width, height)
{

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
MainWindow::~MainWindow()
{

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void MainWindow::createScene()
{
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f));

    Ogre::Entity *sphere = mSceneMgr->createEntity("Sphere", Ogre::SceneManager::PT_SPHERE);

    Ogre::SceneNode *childSceneNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();

    childSceneNode->attachObject(sphere);

    Ogre::MaterialPtr sphereMaterial =
            Ogre::MaterialManager::getSingleton().create("SphereMaterial",
                                                         Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                         true);

    sphereMaterial->getTechnique(0)->getPass(0)->setAmbient(0.1f, 0.1f, 0.1f);
    sphereMaterial->getTechnique(0)->getPass(0)->setDiffuse(0.9f, 0.2f, 0.2f, 1.0f);
    sphereMaterial->getTechnique(0)->getPass(0)->setSpecular(0.9f, 0.9f, 0.9f, 1.0f);

    sphere->setMaterialName("SphereMaterial");

    childSceneNode->setPosition(Ogre::Vector3(0.0f, 0.0f, 0.0f));
    childSceneNode->setScale(Ogre::Vector3(0.02f, 0.02f, 0.02f));

    Ogre::Light *light = mSceneMgr->createLight("MainLight");
    light->setPosition(20.0f, 80.0f, 50.0f);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void MainWindow::createCamera()
{
    QtOgreBaseWindow::createCamera();

    mCameraMan = new OgreQtBites::QtCameraMan(mCamera);
    mCameraMan->setTopSpeed(15);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool MainWindow::frameRenderingQueued(const Ogre::FrameEvent &evt)
{
    return mCameraMan->frameRenderingQueued(evt);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void MainWindow::keyPressEvent(QKeyEvent *ev)
{
    if (ev->key() == Qt::Key_Escape)
        QApplication::exit();
    else if (mCameraMan)
        mCameraMan->injectKeyDown(*ev);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void MainWindow::keyReleaseEvent(QKeyEvent *ev)
{
    if (mCameraMan)
        mCameraMan->injectKeyUp(*ev);
}


