#-------------------------------------------------------------------------------
#
#       �������� ������ ��� qmake
#
#
#-------------------------------------------------------------------------------

# ��� ������ � ��� ������������ �����
TEMPLATE = app
TARGET = QtOgreBase

# ���������� ������ Qt
QT += core gui
QT += widgets

# ������������ ������ ��� unix
unix {

    INCLUDEPATH += /usr/include/OGRE
    INCLUDEPATH += /usr/include/OGRE/Overlay
    INCLUDEPATH += /usr/include/OGRE/Terrain

    CONFIG += link_pkgconfig
    PKGCONFIG += OGRE
}

# ������������ ������ ��� �����
win32 {

}


CONFIG(debug, debug | release) {
    # ������ ������� ��� ����������� ���������
    TARGET = $$join(TARGET,,,_d)
    # ���������� ������ ���������
    LIBS *= -lOgreMain -lOIS -lOgreOverlay -lboost_system
    # ���� � ������������ �����
    DESTDIR = ../bin

} else {
    # ����������� ��������� ��� ������
    LIBS *= -lOgreMain -lOIS -lOgreOverlay -lboost_system
    DESTDIR = ../bin
}

# ���� ������ ����������
INCLUDEPATH += include/

# ������ ������������ ������
HEADERS += $$files(include/*.h)

# ��������� �������
SOURCES += $$files(src/*.cpp)
