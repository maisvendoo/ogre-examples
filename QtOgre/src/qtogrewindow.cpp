//------------------------------------------------------------------------------
//
//      ����� ����, � ������� ��������� ��������� OGRE
//      (c) �������� �.�., 28/12/2016
//
//------------------------------------------------------------------------------
#include    "qtogrewindow.h"

const   QString     CFG_PATH = "../cfg/";
const   QString     LOG_PATH = "../logs/";

const   int         WIN_WIDTH = 1024;
const   int         WIN_HEIGHT = 768;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
QtOgreWindow::QtOgreWindow(QWindow *parent) :
    QWindow(parent),
    root(NULL),
    ogreWindow(NULL),
    update_pending(false)

{
    // ������������� ������ �������. ���������� eventFilter() ������� ����
    // ����� ������������ ������� ���������� ����� �� ����
    installEventFilter(this);

    // ������� ����
    this->setWidth(WIN_WIDTH);
    this->setHeight(WIN_HEIGHT);

    // ���� ���� ���� �������
    ogre_background = Ogre::ColourValue(0.0f, 0.5f, 1.0f);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
QtOgreWindow::~QtOgreWindow()
{
    // ���������� �������� ������� �����
    delete root;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::render(QPainter *painter)
{
    Q_UNUSED(painter);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::render()
{
    Ogre::WindowEventUtilities::messagePump();
    root->renderOneFrame();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::initialize()
{
    // ���� � ������ ������������
    string cfg_path = CFG_PATH.toStdString();
    // ���� � �����
    string log_path = LOG_PATH.toStdString();

    //---- ������� �������� ����� �����, ������ ��������� ����������������
    // ������ � �����
    root = new Ogre::Root(Ogre::String((cfg_path + "plugins.cfg").c_str()),
                          Ogre::String((cfg_path + "ogre.cfg").c_str()),
                          Ogre::String((log_path + "Ogre.log").c_str()));

    //---- �������� ��������

    // ��������� ������
    Ogre::ConfigFile cf;
    cf.load(cfg_path + "resources.cfg");

    // ���������� ������ �������, ���������� �������������� ��������
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    Ogre::String secName;
    Ogre::String typeName;
    Ogre::String archName;

    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;

        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;

            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName,
                                                                           typeName,
                                                                           secName);
        }
    }

    //---- ���������� ��������� ����������� �������

    // ���������� ������ ��������� ��������
    const Ogre::RenderSystemList &rsList = root->getAvailableRenderers();

    // ����� ��� ���������� ���������� �������
    Ogre::RenderSystem *rs = rsList[0];

    // ������ ��������� ��������
    Ogre::StringVector renderOrder;

    // ���� �������� ��� ������, ��������� ����������� ����������
    // � ������� Direct3D
#if defined(Q_OS_WIN)

    renderOrder.push_back("Direct3D9");
    renderOrder.push_back("Direct3D11");

#endif

    // �� � ����� ������ ������������� OpenGL ��� �������
    renderOrder.push_back("OpenGL");
    renderOrder.push_back("OpenGL 3+");

    // ���� ���������� ������
    Ogre::StringVector::iterator iter;

    // ���������� ��� ������� �� ������
    for (iter = renderOrder.begin(); iter != renderOrder.end(); iter++)
    {
        Ogre::RenderSystemList::const_iterator it;

        // ���������� ��� ��������� � ������� �������
        for (it = rsList.begin(); it != rsList.end(); it++)
        {
            if ((*it)->getName().find(*iter) != Ogre::String::npos)
            {
                rs = *it;
                break;
            }
        }

        if (rs != NULL)
            break;
    }

    // ���� ������ �� ������
    if (rs == NULL)
    {
        // �������� �������� ������
        if (!root->restoreConfig())
        {
            // ���� �� ������ ������, ������������� ������������ �����������
            // ������� ������ ��������������
            if (!root->showConfigDialog())
            {
                // � ������ ������� ���������� ����������
                OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS,
                            "Abort render system configuration",
                            "QtOgreWindow::initialize");
            }
        }
    }

    //---- ������������� ��������� ����

    // ����� � ������ �������� ���� ���������� Qt
    QString dim = QString("%1 x %2").arg(this->width()).arg(this->height());

    // ������������� ����������
    rs->setConfigOption("Video Mode", dim.toStdString());
    // ������������� ���� �������������� ������
    rs->setConfigOption("Full Screen", "No");
    // �������� ������������ �������������
    rs->setConfigOption("VSync", "Yes");

    // ������ ������� ���������� ��� OGRE
    root->setRenderSystem(rs);
    // �������������� �����
    root->initialise(false);

    Ogre::NameValuePairList params;

    if (rs->getName().find("GL") <= rs->getName().size())
        params["currentGLContext"] = Ogre::String("false");

#if defined(Q_OS_WIN)

    params["externalWindowHandle"] =
            Ogre::StringConverter::toString((size_t) (this->winId()));

    params["parentWindoHandle"] =
            Ogre::StringConverter::toString((size_t) (this->winId()));

#else

    params["externalWindowHandle"] =
            Ogre::StringConverter::toString((unsigned long) (this->winId()));

    params["parentWindoHandle"] =
            Ogre::StringConverter::toString((unsigned long) (this->winId()));

#endif

    ogreWindow = root->createRenderWindow("QT Window",
                                          this->width(),
                                          this->height(),
                                          false,
                                          &params);

    ogreWindow->setVisible(true);

    //---- ������� �������� �����
    scnMgr = root->createSceneManager(Ogre::ST_GENERIC);

    //---- ������� ������
    Ogre::Camera *ogreCamera = scnMgr->createCamera("MainCamera");
    ogreCamera->setPosition(Ogre::Vector3(0.0f, 0.0f, 10.0f));
    ogreCamera->lookAt(Ogre::Vector3(0.0f, 0.0f, -300.0f));
    ogreCamera->setNearClipDistance(0.1f);
    ogreCamera->setFarClipDistance(200.0f);

    //---- ������� �������
    Ogre::Viewport *pViewPort = ogreWindow->addViewport(ogreCamera);
    // ������������� ���� ����
    pViewPort->setBackgroundColour(ogre_background);
    // ��������� ������
    ogreCamera->setAspectRatio(Ogre::Real(ogreWindow->getWidth()) / Ogre::Real(ogreWindow->getHeight()));
    // �������������� ��������� ������� ��� ��������� �������� ����
    ogreCamera->setAutoAspectRatio(true);

    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    //---- ������� ������� �����
    createScene();

    //---- ���������� ���������� ������� ����
    root->addFrameListener(this);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::createScene()
{
    // ������ �����
    scnMgr->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f));

    Ogre::Entity *sphere = scnMgr->createEntity("Sphere", Ogre::SceneManager::PT_SPHERE);

    Ogre::SceneNode *childSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();

    childSceneNode->attachObject(sphere);

    Ogre::MaterialPtr sphereMaterial =
            Ogre::MaterialManager::getSingleton().create("SphereMaterial",
                                                         Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                         true);

    sphereMaterial->getTechnique(0)->getPass(0)->setAmbient(0.1f, 0.1f, 0.1f);
    sphereMaterial->getTechnique(0)->getPass(0)->setDiffuse(0.9f, 0.2f, 0.2f, 1.0f);
    sphereMaterial->getTechnique(0)->getPass(0)->setSpecular(0.9f, 0.9f, 0.9f, 1.0f);

    sphere->setMaterialName("SphereMaterial");

    childSceneNode->setPosition(Ogre::Vector3(0.0f, 0.0f, 0.0f));
    childSceneNode->setScale(Ogre::Vector3(0.02f, 0.02f, 0.02f));

    Ogre::Light *light = scnMgr->createLight("MainLight");
    light->setPosition(20.0f, 80.0f, 50.0f);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::renderLater()
{
    if (!update_pending)
    {
        update_pending = true;
        QApplication::postEvent(this, new QEvent(QEvent::UpdateRequest));
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::renderNow()
{
    // �������, ���� ���� �� ������������
    if (!isExposed())
        return;

    // ���� �� ������ �������� ���� �����
    if (root == NULL)
    {
        // ��������� �������������
        initialize();
    }

    // ��������� ���������
    render();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtOgreWindow::eventFilter(QObject *watched, QEvent *event)
{
    // ���� ����������� ������ - ���� ����
    if (watched == this)
    {
        // ���� ��� ������� - ��������� ��������
        if (event->type() == QEvent::Resize)
        {
            // ���� ���� ������������ � ���������� ���� OGRE
            if ( isExposed() && (ogreWindow != NULL) )
            {
                // ������ ����� ������ ����
                ogreWindow->resize(this->width(), this->height());
            }
        }
    }

    return false;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::keyPressEvent(QKeyEvent *ev)
{
    Q_UNUSED(ev);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::keyReleaseEvent(QKeyEvent *ev)
{
    Q_UNUSED(ev);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::exposeEvent(QExposeEvent *event)
{
    // �������� �������� event ��� �������������� � ���� ������� ������
    Q_UNUSED(event);

    // ���� ���� ������������, �������� ����������
    if (isExposed())
        renderNow();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtOgreWindow::event(QEvent *event)
{
    // ���������� ��� ��������� �������
    switch (event->type())
    {
    // ������� - ������ ������ ���� �����������
    case QEvent::UpdateRequest:

        // ���������� ���� �������� ����������
        update_pending = false;
        // ������������ ����
        renderNow();
        // ���������� ���� �������� ���������
        return true;

    default:

        // ��� ����������� ������� �������, �������� ����������
        // ������ QWindow, ��������� ��� �������� �������
        return QWindow::event(event);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtOgreWindow::frameRenderingQueued(const Ogre::FrameEvent &evt)
{
    Q_UNUSED(evt);

    return true;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::log(Ogre::String msg)
{
    if (Ogre::LogManager::getSingletonPtr() != NULL)
        Ogre::LogManager::getSingletonPtr()->logMessage(msg);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreWindow::log(QString msg)
{
    log(Ogre::String(msg.toStdString().c_str()));
}

















