#-------------------------------------------------------------------------------
#
#       Сценарий сборки для qmake
#
#
#-------------------------------------------------------------------------------

# Тип сборки и имя исполняемого файла
TEMPLATE = app
TARGET = QtOgre

# Включаемые модули Qt
QT += core gui
QT += widgets

# Конфигурация сборки для unix
unix {

    INCLUDEPATH += /usr/include/OGRE
    CONFIG += link_pkgconfig
    PKGCONFIG += OGRE
}

# Конфигурация сборки для винды
win32 {

}


CONFIG(debug, debug | release) {
    # Задаем суффикс для отладочного бинарника
    TARGET = $$join(TARGET,,,_d)
    # Отладочные версии библиотек
    LIBS *= -lOgreMain -lOIS -lOgreOverlay -lboost_system
    # Путь к исполняемому файлу
    DESTDIR = ../bin

} else {
    # Аналогичные настройки для релиза
    LIBS *= -lOgreMain -lOIS -lOgreOverlay -lboost_system
    DESTDIR = ../bin
}

# Путь поиска заголовков
INCLUDEPATH += include/

# Список заголовочных файлов
HEADERS += $$files(include/*.h)

# Исходники модулей
SOURCES += $$files(src/*.cpp)
