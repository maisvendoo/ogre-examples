//------------------------------------------------------------------------------
//
//      ����� ����, � ������� ��������� ��������� OGRE
//      (c) �������� �.�., 28/12/2016
//
//------------------------------------------------------------------------------
#ifndef QTOGREWINDOW_H
#define QTOGREWINDOW_H

#include    <iostream>

#include    <QtWidgets/QApplication>
#include    <QtGui/QKeyEvent>
#include    <QtGui/QWindow>

#include    <Ogre.h>

using namespace std;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class QtOgreWindow : public QWindow, Ogre::FrameListener
{
Q_OBJECT

public:

    // ����������� � ���������� ����
    explicit QtOgreWindow(QWindow *parent = NULL);
    virtual ~QtOgreWindow();

    virtual void render(QPainter *painter);
    // ��������� �����
    virtual void render();
    // ������������� ����������
    virtual void initialize();
    // �������� �������� �����
    virtual void createScene();

public slots:

    virtual void renderLater();
    virtual void renderNow();

    // ��������� ������� ����
    virtual bool eventFilter(QObject *watched, QEvent *event);

signals:

    void entitySelected(Ogre::Entity *entity);

protected:

    // �������� ���� �����
    Ogre::Root          *root;
    // ������ ����
    Ogre::RenderWindow  *ogreWindow;
    // �������� �����
    Ogre::SceneManager  *scnMgr;
    // ���� �������� ���������� ����
    bool                update_pending;
    // ���� ���� ����
    Ogre::ColourValue   ogre_background;

    // ���������� ������� �������
    virtual void keyPressEvent(QKeyEvent *ev);
    // ���������� �������� �������
    virtual void keyReleaseEvent(QKeyEvent *ev);
    // ����������, ����� ���� ������������ �������
    virtual void exposeEvent(QExposeEvent *event);

    // ���������� ���� �������, ���������� ����
    // ���������� true, ���� ������� ���������� � ����������
    virtual bool event(QEvent *event);

    // ���������� ����� ��������� ���� �������� �����, ��
    // �� ������������ �������. ���������� �� ��������� �
    // ���������� ������� � ������ �����
    virtual bool frameRenderingQueued(const Ogre::FrameEvent &evt);

    void log(Ogre::String msg);
    void log(QString msg);
};

#endif // QTOGREWINDOW_H
